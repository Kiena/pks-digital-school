<?php

    // Sheep
    require('animal.php');

    $sheep = new Animal("shaun");

    echo "Nama : ". $sheep->name. "<br>"; 
    echo "Kaki : ". $sheep->legs. "<br>"; 
    echo "Berdarah Dingin :". $sheep->cold_blooded. "<br>"; 

    // Frog
    require('Frog.php');

    $frog = new Frog("Buduk");

    echo "<br>". "Nama : ". $frog->name. "<br>"; 
    echo "Kaki : ". $frog->legs. "<br>"; 
    echo "Berdarah Dingin :". $frog->cold_blooded. "<br>"; 
    echo "Jump :". $frog->jump. "<br>"; 

    // Ape
    require('Ape.php');

    $ape = new Ape("Kera Sakti");

    echo "<br>". "Nama : ". $ape->name. "<br>"; 
    echo "Kaki : ". $ape->legs. "<br>"; 
    echo "Berdarah Dingin :". $ape->cold_blooded. "<br>";

?>
